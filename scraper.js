// inital setup

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var logger = require('morgan');
var minify = require('express-minify');
var timeout = require('connect-timeout');
var loadModules = require(__dirname + '/server/lib/helpers/loadModules');
var sqlConnect = require(__dirname + '/server/mysql_connects/db_conf');
GLOBAL.modules = {};
var module_directory = __dirname+"/server/lib/scrapeTargets";
//var compress = require('compression');
GLOBAL.router = express.Router();
var port = process.env.PORT || 9876;

// configuration ===============================================================

//load all functions
loadModules(module_directory);

if(process.env.NODE_ENV=='development'){
	//app.use(compress({threshold: 256}));
	app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
	app.use(logger('combined')); 						// log every request to the console
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	var db_init = {
		host     : 'localhost',
		user     : 'root',
		password : '',
		database : ''
	};
	sqlConnect.mysqlConnect(db_init);
}else if(process.env.NODE_ENV=='production'){
	//app.use(compress({threshold: 256}));
	app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
	app.use(logger()); 								// log every request to the console - default settings
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	var db_init = {
		host     : '3a7ff4b3d827722adc5ff9eaf30f56e25d924b46.rackspaceclouddb.com',
		user     : 'mymugzy',
		password : 'mymugzy!2009',
		database : ''
	};
	sqlConnect.mysqlConnect(db_init);
}else{
	app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
	app.use(logger('combined')); 						// log every request to the console
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	var db_init = {
		host     : '44fe862ed36373986601eac2094f12ad2d6c3c8c.rackspaceclouddb.com',
		user     : 'mymugzy',
		password : 'mymugzy!2009',
		database : ''
	};
	sqlConnect.mysqlConnect(db_init);
}
app.use(timeout('1200s'));

//configure CORs
app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
});
//load the routes
require('./server/routes')(GLOBAL.router);

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be from root /
app.use('/', GLOBAL.router);

//start our server
app.listen(port);
console.log('starting server at port '+port);