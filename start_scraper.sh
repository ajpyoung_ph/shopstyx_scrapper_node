#!/bin/bash

if [ -n "$1" ] && [ -n "$2" ]
# Test whether command-line argument is present (non-empty).
then
	NODE_ENV=$1 $2 scraper.js
fi

if [ -n "$1" ] && [ -z "$2" ]
then
	NODE_ENV=$1 node scraper.js
fi

if [ -z "$1" ] && [ -z "$2" ]
then
	NODE_ENV=development node-debug scraper.js
fi