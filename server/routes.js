var scraper = require(__dirname + '/lib/scraping_process');
var search = require(__dirname + '/lib/search_process');

module.exports = function(router){
	router.route('/')
		.get(function(req, res) {
			res.send('Page your trying to reach is unknown');
		});

	//get single product
	router.route('/gsp/:user_id/:token/:url')
		.get(function(req,res){
			var token_need = true;
			if(process.env.NODE_ENV=='development')
			{
				token_need = false;
			}//if not development environment then do not check for token
			if(token_need)
			{
				//check token
				var query = 'SELECT * FROM cs_tokens WHERE user_id='+req.params.user_id+' AND token = "'+req.params.token+'";'
				GLOBAL.db_cs_common.query(query,function(err,rows, fields){
					if(err==null)
					{
						if(rows.length > 0)
						{
							scraper.scrapeSite(req.params.url,res,req);
						}else{
							var message = {
								code: "TNOTFOUND",
								errno: "TNOTFOUND",
								syscall: "none",
								message: "Security Code and Security ID does not match"
							};
							res.json(message);
						}
					}
				});
			}else{
				scraper.scrapeSite(req.params.url,res,req);
			}
		});

	//search route	
	router.route('/search/:keyword/:limit/:page/:domain')
		.get(function(req,res){
			var isNumber = false;
			if( ((typeof req.params.limit)=='number' && isFinite(req.params.limit) && req.params.limit%1===0) && ((typeof req.params.page)=='number' && isFinite(req.params.page) && req.params.page%1===0) )
			{
				isNumber=true;
			}
			if(isNumber)
			{
				search.searchKeyword(req,res);
			}else{
				var message = {
					code: "TNOTFOUND",
					errno: "TNOTFOUND",
					syscall: "none",
					message: "limit or page parameter is not a number"
				};
				res.json(message);
			}
		});
}