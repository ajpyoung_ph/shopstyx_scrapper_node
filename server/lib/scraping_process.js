var Crawler = require ('crawler').Crawler;
var parseUri = require(__dirname + '/parseUri');
var txtman = require(__dirname + '/helpers/text_manipulation');
var mysql = require(__dirname + '/helpers/mysql_helpers');
var imageFilters = ['.jpg'];
var sync = require('synchronize');
var JSONstructs = require(__dirname + '/helpers/standardJSONstruct');
var dataSend = JSONstructs.dataSend;


module.exports.scrapeSite = function(url,myres,myreq){

	//check DB first
	data={
		"url":url,
		"searchby":"url" 
	};
	var ret = false;
	query = mysql.getExternalProducts(data);
	GLOBAL.db_cs_search_results.query(query,function(err,rows, fields){
		if(err==null)
		{
			if(rows.length > 0)
			{
				//populate dataSend
				var images = eval("("+rows[0].image_urls+")");
				dataSend = {
					"product_name":rows[0].name,
					"mainDescription":"",
					"price":rows[0].price,
					"image":rows[0].image_url,
					"images": images,
					"otherTitle":[],
					"otherDescription":[],
					"otherPrices":[],
					"site_name":rows[0].domain,
					"web_url":rows[0].url,
					"canonical_url":rows[0].canonical_url,
					"meta_keywords":rows[0].meta_keywords
				};
				myres.json(dataSend);
			}else{
				//if no result then scrape
				var c = new Crawler({
					"maxConnections":10,
					"callback":function(err,result,$){
						parsePage(err,result,$,myres,myreq);
					},
					"forceUTF8": true
				});
				c.queue(url);
			}
		}else{
			console.log("MySQL Error:");
			console.log(error);
		}

	});
}

function parsePage(err,result,$,res,req){
	if(err==null)
	{
		//result.uri;//this is the original URL sent
		//result.body;//this is the read information
		//console.log(result.body);
		//get domain
		var domain = parseUri(result.uri).host;
		//remove www from domain
		dataSend.site_name = domain.replace("www.","");
		//get original url
		dataSend.web_url = result.uri;
		//get canonical
		if((dataSend.canonical_url = txtman.getCanonical($,result.body))=='')
		{
			dataSend.canonical_url = "";
		}
		//get keywords
		if((dataSend.meta_keywords = txtman.getKeywords($,result.body))=='')
		{
			dataSend.meta_keywords = "";
		}
		//use domain as the function
		try{
			var asos = /asos.com/;
			var shopify = /shopify.com/;
			if(asos.test(domain))
			{
				GLOBAL.modules["wc.asos.com"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
			}else if(shopify.test(domain)){
				GLOBAL.modules["wc.shopify.com"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
			}else{
				GLOBAL.modules[domain](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
			}
		}catch(error){
			console.log(error);
			GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
		}
	}else{
		console.log("Error in parsing");
		console.log(err);
		res.json(err);
		/*
		No internet connection
		{
			code: "ENOTFOUND",
			errno: "ENOTFOUND",
			syscall: "getaddrinfo"
		}
		*/
	}
}

