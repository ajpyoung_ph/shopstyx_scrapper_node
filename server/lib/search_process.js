var mysql = require(__dirname + '/helpers/mysql_helpers');
var JSONstructs = require(__dirname + '/helpers/standardJSONstruct');
var bp = require(__dirname+'/bing_process');
var dataSend = JSONstructs.dataSend;
var txtman = require(__dirname + '/helpers/text_manipulation');


module.exports.searchKeyword = function(req,res){
	var keywords = req.params.keyword;
	var limit = req.params.limit;
	var page = req.params.page;	
	var domain = (req.params.domain).trim();

	var post_data = {
		"keyword":keywords,
		"offset":page,
		"limit":limit,
		"domain":domain
	};

	var domain_query = '';
	if(domain == '' || domain == null || domain == "null" || domain == undefined)
	{
		domain_query = '';
	}else{
		domain_query = " AND domain = '"+txtman.mysql_real_escape_string(data['domain'])+"'";
	}
	var query = "SELECT * FROM cs_search_results WHERE MATCH (keywords, meta_keywords) AGAINST ('"+txtman.mysql_real_escape_string(data['keyword'])+"' IN BOOLEAN MODE) "+domain_query+" AND is_valid = 1 ORDER BY id ASC LIMIT "+data['offset']+","+data['limit']+";";

	GLOBAL.db_cs_search_results.query(query,function(err,rows, fields){
		if(err==null)
		{
			var results = [];
			if(rows.length < 1)
			{
				//no results
				var color = '';
				var size = '';
				bp.bingsearchproducts(data['offset'], data['limit'], data['keyword'], data['domain'], color, size, req,res);
				//_bingsearchproducts


			}else{
				//has results
				var counter=0;
				rows.forEach(function(data){
					results[counter]["product_name"]=data.name;
					results[counter]["price"]=parseFloat(data.name.replace(/,/g, ''));
					results[counter]['image'] = data.image_url;
					results[counter]['images'] = data.image_urls;
					results[counter]['web_url'] = (data.url).trim;
					results[counter]['canonical_url'] = (data.canonical_url).trim;
					results[counter]['site_name'] = data.domain;
					results[counter]['meta_keywords'] = data.meta_keywords;
					counter++;
				});
				var message = {
					data_status:true,
					data_type:"db",
					data:JSON.encode(results)
				}
			}
		}else{
			console.log("Error in SQL query");
			console.log(query);
			console.log(err);
			res.json(err);
		}
	});
}