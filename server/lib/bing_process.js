var Crawler = require ('crawler').Crawler;


module.exports.bingsearchproducts = function(offset, limit, keywords, site, color, size, req,res){
	var results = '';
	var search_text = keywords;
	var finalquery = [];

	if(color!='')
	{
		finalquery.push(color);
	}
	if(size!='')
	{
		finalquery.push(size);
	}
	if(site != '')
	{
		switch(site)
		{
			case 'amazon.com':
				finalquery.push('prefer:Price prefer:Sale NOT inbody:"Currently unavailable." site:amazon.com NOT site:askville.amazon.com NOT inurl:/dp/images/');
				break;
			case 'etsy.com':
				finalquery.push('-"Sorry, this item sold."');
				finalquery.push('site:'+site);
				break;
			default:
				finalquery.push(site);
				finalquery.push('site:'+site);
				break;
		}
	}
	var finalString = finalquery.join(" ");
	var address = '';
	dataConstruct = {
		"offset":offset,
		"limit":limit,
		"keywords":keywords,
		"site":site,
		"color":color,
		"size":size,
		"req":req,
		"res":res,
		"query":finalString
	};
	imagesearch(dataConstruct,processImageResultsBingSearchProducts);
};

function imagesearch(dataConstruct, callback)
{
	var query = dataConstruct.query;
	var offset = dataConstruct.offset;
	var limit = dataConstruct.limit;
	var longitude = '';
	var latitude = '';

	var url = "https://api.datamarket.azure.com/Bing/Search/v1/Composite?Sources=%27image%2Brelatedsearch%27&Query=%27"+query+"%27"+skip+latitude+longitude+"&\$format=json&\$top="+limit;
	var key = "h/covmZGq4PKRkhcK9xx0H4qb+ngwHoE381LMO7C1E8";

	var c = new Crawler({
		"maxConnections":10,
		"callback":function(err,result,$){
			parsePage(err,result,$,dataConstruct,callback);
		},
		"forceUTF8": true,
		"userAgent": "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)",
		"auth" : {
			key+":"+key,
			'sendImmediately': false
		},
		"followRedirect":true,
		"tunnel":true
	});

	c.queue(url);
}

function parsePage(err,result,$,dataConstruct,callback)
{
	if(err==null)
	{
		results = {
			"err":err,
			"result":result,
			"$":$
		}
		callback(dataConstruct,results);
	}else{
		console.log("Error in parsePage for imagesearch");
		console.log(err);
		dataConstruct.res.json(err);
	}
}

function processImageResultsBingSearchProducts(dataConstruct,orig_results)
{
	var setup = 0;
	var pull=1;
	var sendExternal = false;
	try{
		if(dataConstruct["results"]==undefined)
		{
			setup = dataConstruct["processImageResultsBingSearchProducts_i"];
			results = dataConstruct["processImageResultsBingSearchProducts_results"];
			tmp2 = dataConstruct["processImageResultsBingSearchProducts_tmp2"];
			pull = 0;
		}
	}catch(error){
		//do nothing
		pull = 1;
	}
	if(pull)
	{
		var tmp = JSON.encode(results.result.body);
		var results = tmp.d.results[0].Image;
		var tmp2 = [];
	}	
		var filter_status = false;
	// if(filter_status==false)
	// {
		for(var i = setup; i < results.length; i++)
		{
			var SourceUrl = results[i].SourceUrl;
			var site = dataConstruct.site;
			if( (SourceUrl.indexOf('/dp/images') == -1) && (SourceUrl.indexOf('&node=') == -1) && (site.indexOf('amazon.com') != -1) )
			{
				tmp2.push(results[i]);
				filter_status = true;
			}
			if( (site.indexOf('zappos.com') != -1) && (SourceUrl.indexOf('.zml') == -1) && (SourceUrl != 'http://www.zappos.com') )
			{
				tmp2.push(results[i]);
				filter_status = true;
			}
			if(site.indexOf('ebay.com') != -1)
			{
				dataConstruct["processImageResultsBingSearchProducts_i"]=i;
				dataConstruct["processImageResultsBingSearchProducts_results"]=results;
				dataConstruct["processImageResultsBingSearchProducts_tmp2"]=tmp2;
				dataConstruct["orig_results"]=orig_results;
				var c = new Crawler({
					"maxConnections":10,
					"callback":function(err,result,$){
						verifyPage(err,result,$,dataConstruct,processImageResultsBingSearchProducts);
					},
					"forceUTF8": true,
					"userAgent": "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)",
					"auth" : {
						key+":"+key,
						'sendImmediately': false
					},
					"followRedirect":true,
					"tunnel":true
				});
				sendExternal=true;
				c.queue(results[i]);
				break;
			}
		}
	// }
	if(sendExternal==false)
	{
		var msg = {
			data_status : true,
			data_type : bing,
			data : []
		};
		if(filter_status)
		{
			msg.data = tmp2;
		}else{
			msg.data = results;
		}
		dataConstruct.res.json(err);	
	}
}

function verifyPage(err,result,$,dataConstruct,callback)
{
	if(err==null)
	{
		if(result.statusCode != 302 || result.statusCode != 301)
		{
			var results = dataConstruct["processImageResultsBingSearchProducts_results"];
			dataConstruct["processImageResultsBingSearchProducts_tmp2"].push(results[dataConstruct["processImageResultsBingSearchProducts_i"]]);
			callback(dataConstruct,dataConstruct["orig_results"]);
		}
	}else{
		console.log("Error in verifyPage for processImageResultsBingSearchProducts");
		console.log(err);
		dataConstruct.res.json(err);
	}
}