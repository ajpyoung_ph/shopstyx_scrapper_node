var txtman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');
var jsonwrt = require ('../helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	
	var dataSource = [];

	dataSource[0] = $('meta[itemprop="name"]').attr("content");
	dataSource[1] = $('span[itemprop="price"]').text();
	if(dataSource[1] == null || dataSource[1] == undefined || dataSource[1] == '')
	{
		var pattern = /<li class="item all trigger">.*?<span class="italic">from<\/span>\s{1,}?<em>\s{1,}?(.*?)\s{1,}?<\/em>/;
		dataSource[1] = txtman.getFirstInstance(pattern,result.body);
	}
	dataSource[2] = $('div.popup-content').children('img').attr("src");

	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
	jsonwrt(dataSend,dataSource);
	res.json(dataSend);
}