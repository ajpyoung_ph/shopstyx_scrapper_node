var imagesize = require('request-image-size');
//var async = require('synchronize');
var textman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');

modules.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	//result.uri;//this is the original URL sent
	//result.body;//this is the read information

	//get price
	//debugger;
	dataSend.otherPrices = textman.getPrice(result.body);
	if(dataSend.otherPrices.length>0)
	{
		dataSend.price = dataSend.otherPrices[0];
	}
	//getting all images		
	var imgList = grphman.getAllImages($,imageFilters);
	dataSend.images = imgList;
	//getting all elements whose id has title as a value
	var idTitle = $("[id]").filter(function() {
		try{
			return(this.id.match(/.*title.*/i) != null);
		}catch(error){
			return false;
		}
	});
	var nameTitle = $("[name]").filter(function() {
	    try{
			return(this.name.match(/.*title.*/i) != null);
		}catch(error){
			return false;
		}
	});
	//collect all titles
	var titleList = [];
	textman.getALLtext(idTitle,titleList);
	textman.getALLtext(nameTitle,titleList);
	delete idTitle;
	delete nameTitle;
	dataSend.otherTitle=titleList;
	//match $(title).text() if trim(nameTitle.eq(index).text()) matches
	try{
		var titleBarValue = $("title").text();
		dataSend.product_name=titleBarValue;
	}catch(error){
		var titleBarValue = null;
	}
	if(titleBarValue!=null && titleBarValue!=undefined)
	{
		dataSend.product_name = textman.getText(titleList,titleBarValue);
		console.log("product_name :"+dataSend.product_name);
		if(dataSend.product_name==false)
		{
			//if still did not find any title then we use the value in the title bar
			dataSend.product_name=titleBarValue;
		}
	}else{
		//if there's no titleBarValue then this must be the worst site ever so I'll just use the first text value available
		var dummyvar = [""];
		dataSend.product_name = textman.findtext(titleList);
	}
	//getting all elements whose name has title as a value
	//pulling image dimensions
	// async.mapSeries(imgList, imagesize, function(err,dim){
	// 	//console.log(err,dim);
	// 	//getting the biggest picture
	// 	var currentDim = 0;
	// 	var maxDim = 0;
	// 	for(var x=0;x<dim.length;x++)
	// 	{
	// 		currentDim = parseFloat(dim[x].height * dim[x].width);
	// 		if(currentDim>maxDim)
	// 		{
	// 			maxDim=currentDim;
	// 			dataSend.mainPic.url = imgList[x];
	// 			dataSend.mainPic.height = dim[x].height;
	// 			dataSend.mainPic.width = dim[x].width;
	// 		}
	// 	}
	// 	//console.log(dataSend);
	// 	GLOBAL.res[domain].json(dataSend);
	// 	delete GLOBAL.res[domain];
	// 	delete GLOBAL.req[domain];
	// });
	res.json(dataSend);
};