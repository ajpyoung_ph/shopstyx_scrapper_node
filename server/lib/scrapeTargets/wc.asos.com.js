//asos.com has multiple subdomains but use the same scraping method
var txtman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');
var jsonwrt = require ('../helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	
	var dataSource = [];
	dataSource[0] = $('meta[itemprop="name"]').attr("content");
	dataSource[1] = $('meta[itemprop="price"]').attr("content");
	dataSource[2] = $('img[itemprop="image"]').attr("src");

	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters);
	jsonwrt(dataSend,dataSource);
	res.json(dataSend);
}