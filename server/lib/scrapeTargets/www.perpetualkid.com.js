var txtman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');
var jsonwrt = require ('../helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	
	var dataSource = [];

	dataSource[0] = $('td[align="center"]').children('p').children('img').attr("alt"); //mainTitle or product_name
	dataSource[1] = $('span.prod-detail-cost-value').text(); //price
	dataSource[2] = $('td[align="center"]').children('p').children('img').attr("src"); //image

	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
	jsonwrt(dataSend,dataSource);
	res.json(dataSend);
}