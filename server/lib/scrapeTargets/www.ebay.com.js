var txtman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');
var jsonwrt = require ('../helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	
	var dataSource = [];

	dataSource[0] = $('meta[property="og:title"]').attr("content");

	dataSource[1] = $('meta[property="og:description"]').attr("content");
	if(dataSource[1]==null || dataSource[1] == undefined || dataSource[1] == '')
	{
		dataSource[1] = $('span.notranslate[itemprop="price"]').text();
		dataSource[1] = dataSource[1].split("US");
		dataSource[1] = dataSource[1][0];
	}else{
		dataSource[1] = dataSource[1].split("US");
		dataSource[1] = dataSource[1][0];
	}
	dataSource[2] = $('img#icImg[itemprop="image"]').attr("src");

	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters);
	jsonwrt(dataSend,dataSource);
	res.json(dataSend);
}