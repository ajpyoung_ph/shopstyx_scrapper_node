var txtman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');
var jsonwrt = require ('../helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	
	var dataSource = [];

	dataSource[0] = $('meta[name="twitter:title"]').attr("content"); //mainTitle or product_name
	dataSource[1] = $('meta[itemprop="price"]').attr("content"); //price
	dataSource[2] = $('img[name="twitter:image"]').attr("src"); //image
	dataSource[2] = dataSource[2].replace('215X215', '500X500');

	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
	jsonwrt(dataSend,dataSource);
	res.json(dataSend);
}