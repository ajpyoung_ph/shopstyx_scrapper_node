var txtman = require ('../helpers/text_manipulation');
var grphman = require ('../helpers/image_manipulation');
var jsonwrt = require ('../helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){

	var dataSource = [];

	dataSource[0] = $("span#btAsinTitle").text();
	if(dataSource[0]==null || dataSource[0]=='' || dataSource[0]==undefined)
	{
		dataSource[0] = $("span#productTitle").text();
	}
	dataSource[1] = $("b.priceLarge").text();
	if(dataSource[1]==null || dataSource[1]=='' || dataSource[1]==undefined)
	{
		dataSource[1] = $("span#priceblock_ourprice").text();
		if(dataSource[1]==null || dataSource[1]=='' || dataSource[1]==undefined)
		{
			dataSource[1] = $("span#current-price").text();
			if(dataSource[1]==null || dataSource[1]=='' || dataSource[1]==undefined)
			{
				dataSource[1] = $("span.a-color-price.offer-price").text();
				try{
					if(dataSource[1].length>1)
					{
						dataSource[1] = dataSource[1][1];
					}
				}catch(error){
					//do nothing?
				}
			}
		}
	}
	dataSource[2] = $("img.kib-ma").attr("src");//ask red about this
	//$image = doParsing($index, '/data-a-dynamic-image.*?quot;(.*?)&quot/s');
	//$image = doParsing($index, '/"large":"(.*?)"/s');
	if(dataSource[2]==null || dataSource[2]=='' || dataSource[2]==undefined)
	{
		dataSource[2] = $("img#imgBlkFront").attr("src");
		if(dataSource[2]==null || dataSource[2]=='' || dataSource[2]==undefined)
		{
			dataSource[2] = $("img[data-a-dynamic-image]").attr("data-a-dynamic-image");
			if(dataSource[2]==null || dataSource[2]=='' || dataSource[2]==undefined)
			{
				var pattern = /"large":"(.*?)"/;
				dataSource[2] = txtman.getFirstInstance(pattern,result.body);
			}else{
				dataSource[2] = dataSource[2].split('"');
				dataSource[2] = dataSource[2][1];
			}
		}
	}
	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters);
	jsonwrt(dataSend,dataSource);
	res.json(dataSend);
}