var mysql = require(__dirname +"/mysql_helpers");

module.exports = function(dataSend,dataSource)
{
	dataSend.product_name = dataSource[0].trim();
	dataSend.price = dataSource[1].trim();
	dataSend.image = dataSource[2].trim();
	dataSend.images = dataSource[3];
	mysql.saveNewDataSend(dataSend);
}