

module.exports.getText = function(sourceArray,titleBarValue)
{
	for(var x = 0; x<sourceArray.length; x++)
	{
		try{
			var value = sourceArray[x];
			value = value.trim();
			if(value!='')
			{
				//add slashes to special chars for RegExp purposes
				var newvalue = value.replace(/([^\w \\])/g,'\\$1');
				var pattern = new RegExp(newvalue,"i");
				if(pattern.test(titleBarValue))
				{
					console.log("Chosen Title: "+value);
					return value;
					break;
				}else{
					console.log(value+" not in "+titleBarValue);
				}
			}
		}catch(error){
			//do nothing
		}
	}
	return false;
}

module.exports.findtext = function(sourceArray)
{
	for(var x = 0; x<sourceArray.length; x++)
	{
		try{
			var value = sourceArray[x].trim();
			return value;
		}catch(error){
			//do nothing
		}
	}
	return "No Title Found";
}

module.exports.getALLtext = function(sourceArray,targetArray)
{
	for(var x = 0; x<sourceArray.length; x++)
	{
		try{
			var txt = sourceArray.eq(x).text();
			if(txt!='')
			{
				txt = txt.trim();
				targetArray.push(txt);
			}
			// var html = sourceArray.eq(x).html();
			// if(html!='')
			// {
			// 	html = html.trim();
			// 	targetArray.push(html);
			// }
			var value = sourceArray.eq(x).val();
			if(value!='')
			{
				value = value.trim();
				targetArray.push(value);
			} 
		}catch(error){
			//do nothing
		}
	}
}

module.exports.getPrice = function(body)
{
	var temp = '';
	var arrayRet = [];
	var currPattern1 = /(AUD|$|EUR|€|GBP|£|USD|$|JPY|¥|PHP|₱|HKD|$){1}( ){0,1}\d{1,3}(?:(,)(\d){1,3})*(\.\d{2}){1}/gi;
	var currPattern2 = /(AUD|$|EUR|€|GBP|£|USD|$|JPY|¥|PHP|₱|HKD|$){0,1}( ){0,1}\d{1,3}(?:(,)(\d){1,3})*(\.\d{2}){1}/gi;
	do{
		temp = currPattern1.exec(body);
		if(temp){
			//console.log(temp[0]);
			arrayRet.push(temp[0]);
		}
		//console.log("still processing css "+title);
	}while(temp)
	if(arrayRet.length<1)
	{
		do{
			temp = currPattern2.exec(body);
			if(temp){
				//console.log(temp[0]);
				arrayRet.push(temp[0]);
			}
			//console.log("still processing css "+title);
		}while(temp)
	}
	arrayRet.sort(function(a, b){
	  return b.length - a.length; // ASC -> a - b; DESC -> b - a
	});	
	return arrayRet;
}

module.exports.getFirstInstance=function(RegExStatement,body)
{
	return RegExStatement.exec(body);
}

module.exports.getCanonical=function($,body)
{
	var canonical = $('link[rel="canonical"]').attr("href");
	if(canonical==null||canonical==''||canonical==undefined)
	{
		canonical = $('meta[property="og:url"]').attr("content");
	}
	return canonical;
}

module.exports.getKeywords=function($,body)
{
	var keywords = $('meta[name="keywords"]').attr("content");
	return keywords;
}

module.exports.stripslashes=function(str) 
{
  //       discuss at: http://phpjs.org/functions/stripslashes/
  //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      improved by: Ates Goral (http://magnetiq.com)
  //      improved by: marrtins
  //      improved by: rezna
  //         fixed by: Mick@el
  //      bugfixed by: Onno Marsman
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //         input by: Rick Waldron
  //         input by: Brant Messenger (http://www.brantmessenger.com/)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //        example 1: stripslashes('Kevin\'s code');
  //        returns 1: "Kevin's code"
  //        example 2: stripslashes('Kevin\\\'s code');
  //        returns 2: "Kevin\'s code"

  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
}
module.exports.addslashes=function(str) 
{
  //  discuss at: http://phpjs.org/functions/addslashes/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Ates Goral (http://magnetiq.com)
  // improved by: marrtins
  // improved by: Nate
  // improved by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
  //    input by: Denny Wardhana
  //   example 1: addslashes("kevin's birthday");
  //   returns 1: "kevin\\'s birthday"

  return (str + '')
    .replace(/[\\"']/g, '\\$&')
    .replace(/\u0000/g, '\\0');
}
module.exports.mysql_real_escape_string=function(str) 
{
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

module.exports.mysql_curr_date=function()
{
	return new Date().toMysqlFormat();
} 
/**
 * You first need to create a formatting function to pad numbers to two digits…
 **/
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

/**
 * …and then create the method to output the date string as desired.
 * Some people hate using prototypes this way, but if you are going
 * to apply this to more than one Date object, having it as a prototype
 * makes sense.
 **/
Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

module.exports.removeCurrency=function(str)
{
	return Number(str.replace(/[^0-9\.]+/g,""));
}

module.exports.convertObject=function(str)
{
	var holder = str.slice(1);
	holder = holder.slice(0,holder.length-1);
	var array = holder.split(",");
	return array;
}