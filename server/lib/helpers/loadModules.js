var fs = require('fs');

module.exports = function(directory)
{
	// GLOBAL.modules - our target
	console.log(directory);
	var files = fs.readdirSync(directory);
	var modules = 0;
	try{
		modules = GLOBAL.modules.length;
	}catch(error){
		//do nothing
	}
	if(modules==undefined)
	{
		modules=0;
		GLOBAL.modules.length=0;
	}
	if(modules < files.length)
	{
		files.forEach(function(file){
			console.log(file);
			var filenameCatch = file.split(".js")
			var filename = filenameCatch[0];
			var stats = fs.lstatSync(directory+'/'+file);
			try{
				if(stats.isFile())
				{
					GLOBAL.modules[filename]=require(directory+'/'+file);
					if((typeof GLOBAL.modules[filename]) != 'function')
					{
						GLOBAL.modules[filename] = GLOBAL.modules.exports;
					}
					delete GLOBAL.modules.exports;
					GLOBAL.modules.length++;
				}
			}catch(error){
				console.log(error);
				//skip and do nothing
			}
		});
	}
}