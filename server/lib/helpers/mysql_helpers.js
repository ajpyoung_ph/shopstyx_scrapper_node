var txtman = require(__dirname +"/text_manipulation");
var modman = require(__dirname +"/module_helpers");


module.exports.getExternalProducts = function(data){
	var query = '';
	switch((data.searchby).toLowerCase())
	{
		case 'keywords and domain':
			try{
				if(data['random']!=undefined)
				{
					query = "SELECT * FROM cs_search_results WHERE keywords = '"+txtman.mysql_real_escape_string(data['keywords'])+"' AND domain = '"+txtman.mysql_real_escape_string(data['domain'])+"' AND is_valid = "+data['is_valid']+" ORDER BY RAND() LIMIT "+data['offset']+","+data['limit']+" ";
				}else{
					throw new Error({"message":'Error data["random"] is undefined'});
				}
			}catch(error){
				query = "SELECT * FROM cs_search_results WHERE keywords = '"+txtman.mysql_real_escape_string(data['keywords'])+"' AND domain = '"+txtman.mysql_real_escape_string(data['domain'])+"' AND is_valid = "+data['is_valid']+" ORDER BY last_update ASC LIMIT "+data['offset']+","+data['limit']+" ";
			}
			break;
		case 'keywords':
			query = "SELECT * FROM cs_search_results WHERE keywords = '"+txtman.mysql_real_escape_string(data['keywords'])+"' ORDER BY last_update ASC";
			break;
		case 'url':
		console.log("searching DB");
			query = "SELECT * FROM cs_search_results WHERE url = '"+txtman.mysql_real_escape_string(data['url'])+"' OR canonical_url = '"+txtman.mysql_real_escape_string(data['url'])+"' ";
			break;
		case 'canonical_url':
			query = "SELECT * FROM cs_search_results WHERE canonical_url = '"+txtman.mysql_real_escape_string(data['canonical_url'])+"' ";
			break;
		default:
			return false;
	}
	return query;
}

module.exports.saveNewDataSend = function(dataSend){
	// if(modman.checkURL(dataSend.site_name))
	// {
		// var image_urls = dataSend.images.toString();
		// image_urls = txtman.addslashes(image_urls);
		var query = 'INSERT INTO cs_search_results SET keywords="",	meta_keywords="'+dataSend.meta_keywords+'",	domain="'+dataSend.site_name+'", url="'+dataSend.web_url+'", canonical_url="'+dataSend.canonical_url+'", name="'+dataSend.product_name+'",			price='+txtman.removeCurrency(dataSend.price)+', image_url="'+dataSend.image+'", image_urls=\''+JSON.stringify(dataSend.images)+'\', last_update="'+txtman.mysql_curr_date()+'", is_valid=1;';
		console.log(query);
	    GLOBAL.db_cs_search_results.query(query,function(err,rows, fields){
	    	if(err!=null){
	    		console.log("Error INSERT saveNewDataSend");
	    		console.log(err);
	    	}
	    });
	// }
}

module.exports.insertExternalProductCounts=function(data){
		var query = "INSERT INTO cs_search_results_counter(keywords, domain, counts) VALUES ('"+txtman.mysql_real_escape_string(data['keywords'])+"','"+txtman.mysql_real_escape_string(data['domain'])+"','"+txtman.mysql_real_escape_string(data['counts'])+"')";
		GLOBAL.db_cs_search_results.query(query,function(err,rows,fields){
			if(err!=null){
	    		console.log("Error INSERT insertExternalProductCounts");
	    		console.log(err);
	    	}
		});
}

module.exports.getExternalProductCounts=function(data){
		return "SELECT * FROM cs_search_results_counter WHERE keywords = '"+txtman.mysql_real_escape_string(data['keywords'])+"' AND domain = '"+txtman.mysql_real_escape_string(data['domain'])+"' ";
	}