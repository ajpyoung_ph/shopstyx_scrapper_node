module.exports.getAllImages = function($,imageFilters){
	var imgList = $('img').map(function() {
	    //returning all image tags src
	    if(this.src!='')
	    {
	    	for(var x=0;x<imageFilters.length;x++)
	    	{
	    		var pattern = new RegExp(imageFilters[x],"i");
	    		if(pattern.test(this.src))
	    		{
	    			return this.src;
	    		}
	    	}
	    }
	}).get();
	return imgList;
}